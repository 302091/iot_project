﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using System;
using DatabaseAccessLayer.CoursesLogic;

namespace IotApi.Controllers
{
    /// <summary>
    /// The controllers related to courses
    /// </summary>
    [ApiController]
    [Route("api/iot/course")]
    [AllowAnonymous]
    [EnableCors]
    public class CourseController : ControllerBase
    {
        /// <summary>
        /// Gets the courses a student is on
        /// </summary>
        /// <param name="studentId">the student who's courses are returned, gotten from the header</param>
        /// <returns>a IEnumerable of the students courses,
        /// Revived in Json format</returns>
        [EnableCors()]
        [HttpGet]
        public ActionResult<IEnumerable<Course>> GetStudentsCourses([FromHeader] int studentId)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(studentId.ToString()))
                    return BadRequest("Invalid Id");

                return Ok(CoursesManager.GetCourses(studentId));
            }
            catch (Exception ex)
            {
                return BadRequest("An error has occured.");
            }
        }

        [EnableCors()]
        [HttpGet]
        public ActionResult<IEnumerable<Course>> GetCoursesFromName([FromHeader] string courseDesc)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(courseDesc))
                    return BadRequest("Invalid Id");

                return Ok(CoursesManager.GetCoursesFromName(courseDesc));
            }
            catch (Exception ex)
            {
                return BadRequest("An error has occured.");
            }
        }

    }
}
