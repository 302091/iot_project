﻿using Microsoft.AspNetCore.Mvc;
using Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using System;

namespace IotApi.Controllers    
{
    /// <summary>
    /// The call made if no controller is specified
    /// </summary>
    [ApiController]
    [Route("api/iot")]
    [AllowAnonymous]
    [EnableCors]
    public class DefaultController : ControllerBase
    {
        /// <summary>
        /// Displays a message when the api is called with no controller or input
        /// </summary>
        /// <returns>A message</returns>
        [EnableCors()]
        [HttpGet]
        public ActionResult<string> Get()
        {
            return Ok("Api Available");
        }
    }
}
