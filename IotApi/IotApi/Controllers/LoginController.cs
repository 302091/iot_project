﻿using Microsoft.AspNetCore.Mvc;
using Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using System;
using DatabaseAccessLayer.LoginLogic;
//using System.Web.http.cors;

namespace IotApi.Controllers
{
    /// <summary>
    /// The controllers related to Login functionality 
    /// </summary>
    [ApiController]
    [Route("api/iot/login")]
    [AllowAnonymous]
    //[EnableCors(origins:"localhost:5500", headers:"*", methods:"*")]
    public class LoginController : ControllerBase
    {
        /// <summary>
        /// This finds a user with that email and password and returns their user Id
        /// </summary>
        /// <param name="email">The students email gotten from the header</param>
        /// <param name="password">The students password gotten from the header</param>
        /// <returns>The students Id or "Login Or Password Incorrect."</returns>
        //[EnableCors()]
        [HttpGet]
        public ActionResult<int> Get([FromHeader] string email, [FromHeader] string password)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(email) || string.IsNullOrWhiteSpace(password))
                    throw new Exception("Bad Login Request");
                return Ok(LoginManager.GetUserId(email, password));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// This creates a new student from the json object in the body of the request
        /// </summary>
        /// <param name="student">the new students details gotten from a json object in the body</param>
        /// <returns>The new students Id</returns>
        [EnableCors()]
        [HttpPost]
        public ActionResult<string> Post([FromBody] Student student, [FromHeader] string password)
        {
            try
            {
                if (student.Email == default||
                    student.Name == default ||
                    student.Contact == default ||
                    student.Address == default ||
                    student.DOB == default)
                    return BadRequest("A Field was empty");

                return Ok(CreateNewUser.CreateStudent(student, password));
            }
            catch (Exception)
            {
                return BadRequest("An error has occured.");
            }
        }
    }
}
