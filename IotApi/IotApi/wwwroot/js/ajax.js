//Ajax Request Handler

function loginHandler() {
    var email = document.getElementById("defaultLoginFormEmail").value;
    var password = document.getElementById("defaultLoginFormPassword").value;
    const xhttp = new XMLHttpRequest();

    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            document.cookie = "email=" + email;
            window.location.replace("http://" + window.location.host + "/index.html");
        }

    }
    xhttp.open("GET", "http://localhost:44307/api/iot/login", true);
    xhttp.setRequestHeader("email", email);
    xhttp.setRequestHeader("password", password)
    xhttp.send();
}


function signupHandler() {
    var firstName = document.getElementById("defaultRegisterFormFirstName").value;
    var lastName = document.getElementById("defaultRegisterFormLastName").value;
    var emailAddress = document.getElementById("defaultRegisterFormEmail").value;
    var password = document.getElementById("defaultRegisterFormPassword").value;
    var dateOfB = document.getElementById("defaultRegisterFormDOB").value;
    var address = document.getElementById("defaultRegisterFormAddress").value;
    var phone = document.getElementById("defaultRegisterFormPhone").value;
    var year = dateOfB.slice(4, 8);
    var month = Number(dateOfB.slice(2, 4)) - 1;
    var day = Number(dateOfB.slice(0, 2)) + 1;
    var JSDate_To_Csharp_date = new Date(year, month, day).toJSON();
    var contactToNum = Number(phone);
    var signupPayload = {Name: firstName + " " + lastName, Email: emailAddress, Address: address, DOB: JSDate_To_Csharp_date, Contact: contactToNum, id: 1};
    const xhttp = new XMLHttpRequest();

    xhttp.onreadystatechange = function() {
        if( this.readyState == 4 && this.status == 200) {
            console.log(xhttp.responseText);

        }

    }
    xhttp.open("POST", "http://localhost:44307/api/iot/login", true);
    xhttp.setRequestHeader("Content-Type", "application/json");
    xhttp.setRequestHeader("password", password);
    JSONpayload = JSON.stringify(signupPayload);
    xhttp.send(JSONpayload);
}








//Character limit for type="number", include this function in the html 'oninput'.

function maxNum(obj) {
    if (obj.value.length > obj.max) {
            newVal = obj.value.slice(0, obj.max);
            obj.value = newVal;
    }
}
