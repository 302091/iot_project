﻿CREATE TABLE [User] (
  [Id] int,​
  [UserName] varchar(50),​
  [Password] varchar(50),​
  [LoginTimeStamp] datetime not null,​
  [Count] int,​
  [User_Id] int,​
  PRIMARY KEY ([Id])​
);​
​
CREATE INDEX [FK] ON  [User] ([User_Id]);​
​
CREATE TABLE [Course] (​
  [Id] int,​
  [Code] varchar(10),​
  [Desc] varchar(255),​
  [Type] varchar(50),​
  [length] varchar(255),​
  [Partner_Id] int,​
  [Cost] int,​
  [IsActive] bit,​
  PRIMARY KEY ([Id])​
);​
​
CREATE INDEX [FK] ON  [Course] ([Code], [Partner_Id]);​
​
CREATE TABLE [UserRole] (​
  [User_Id] int,​
  [Role] varchar(255),​
  [IsActive] int,​
  PRIMARY KEY ([User_Id])​
);​
​
CREATE TABLE [Partner] (​
  [Id] int,​
  [Name] varchar(255),​
  [Contact] int,​
  [Address] varchar(255),​
  [Email] varchar(50),​
  [User_Id] int,​
  [IsActive] bit,​
  PRIMARY KEY ([Id])​
);​
​
CREATE INDEX [FK] ON  [Partner] ([User_Id]);​
​
CREATE TABLE [Student] (​
  [Id] int,​
  [Name] varchar(50),​
  [DOB] datetime,​
  [Address] varchar(255),​
  [Contact] bigint,​
  [Email] varchar(50),​
  [Course_Id] int,​
  [Employer_Id] int,​
  [Start_Date] int,
  [End_Date] int,
  [User_Id] int,​
  [IsActive] bit,​
  PRIMARY KEY ([Id])​
);​
​
CREATE INDEX [FK] ON  [Student] ([Course_Id], [Employer_Id], [User_Id]);​
​
CREATE TABLE [Employer] (​
  [Id] int,​
  [Name] varchar(255),​
  [Contact] int,​
  [Address] varchar(255),​
  [Email] varchar(50),​
  [User_Id] int,​
  [IsActive] bit,​
  PRIMARY KEY ([Id])​
);​
​
CREATE INDEX [FK] ON  [Employer] ([User_Id]);​
​
CREATE TABLE [Teacher] (​
  [Id] int,​
  [Name] varchar(50),​
  [Contact] int,​
  [Address] varchar(255),​
  [Email] varchar(50),​
  [Course_Id] int,​
  [User_Id] int,​
  [IsActive] bit,​
  PRIMARY KEY ([Id])​
);​
​
CREATE INDEX [FK] ON  [Teacher] ([Course_Id], [User_Id]);​
​
CREATE TABLE [Admin] (​
  [Id] int,​
  [Name] varchar(50),​
  [Contact] int,​
  [Address] varchar(255),​
  [Email] varchar(50),​
  [Course_Id] int,​
  [User_Id] int,​
  [IsActive] bit,​
  PRIMARY KEY ([Id])​
);​
​
CREATE INDEX [FK] ON  [Admin] ([Course_Id], [User_Id]);​
​
CREATE TABLE [HOD] (​
  [Id] int,​
  [Name] varchar(255),​
  [Contact] int,​
  [Address] varchar(255),​
  [Email] varchar(50),​
  [User_Id] int,​
  [Course_Id] int,​
  [IsActive] bit,​
  PRIMARY KEY ([Id])​
);​
​
CREATE INDEX [FK] ON  [HOD] ([User_Id], [Course_Id]);​