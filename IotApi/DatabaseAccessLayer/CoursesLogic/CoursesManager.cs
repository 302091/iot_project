﻿using System.Collections.Generic;
using System.Linq;
using Models;

namespace DatabaseAccessLayer.CoursesLogic
{
    /// <summary>
    /// Class is Responsible for Managing Course interactions
    /// </summary>
    public static class CoursesManager
    {
        /// <summary>
        /// Functions gets all courses linked to student Id
        /// </summary>
        /// <param name="studentId">The student who's courses are being retrieve</param>
        /// <returns>A list of te students courses</returns>
        public static IEnumerable<Course> GetCourses (int studentId)
        {
            using (DataBaseContext databaseContext = new DataBaseContext())
            {
                IEnumerable<Student> students= databaseContext.Student;
                IEnumerable<Course> courses= databaseContext.Course;
                var studentCourseId = students.First(s => s.User_Id == studentId).Course_Id;
                var studentsCourses = courses.Where(c=> c.Id == studentCourseId);
                return studentsCourses.ToList();
            }
        }

        public static IEnumerable<Course> GetCoursesFromName(string courseDesc)
        {
            using (DataBaseContext databaseContext = new DataBaseContext())
            {
                IEnumerable<Course> courses = databaseContext.Course;
                var studentsCourses = courses.Where(c => c.Desc == courseDesc);
                return studentsCourses.ToList();
            }
        }
    }
}
