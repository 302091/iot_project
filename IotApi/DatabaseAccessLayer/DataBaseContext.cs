﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Models;

namespace DatabaseAccessLayer
{
    /// <summary>
    /// This class contains information to allow access to the database, editing and querying it.
    /// </summary>
    public class DataBaseContext : DbContext
    {
        /// <summary>
        /// This function will set the connection string to access the local database.
        /// </summary>
        /// <param name="optionsBuilder">Helps configure the database connection to use the context.</param>
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            const string connectionString = @"
                                    Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=master;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False
                                ";
            optionsBuilder.UseSqlServer(connectionString);
        }

        public DbSet<User> User { get; set; }
        public DbSet<UserRole> UserRole { get; set; }
        public DbSet<Student> Student { get; set; }
        public DbSet<Teacher> Teacher { get; set; }
        public DbSet<Employer> Employer { get; set; }
        public DbSet<HOD> HOD { get; set; }
        public DbSet<Partner> Partner { get; set; }
        public DbSet<Admin> Admin { get; set; }
        public DbSet<Course> Course { get; set; }
    }


}
