﻿using System;
using System.Collections.Generic;
using System.Linq;
using Models;

namespace DatabaseAccessLayer.LoginLogic
{
    /// <summary>
    /// This class is responsible for making new users
    /// </summary>
    public static class CreateNewUser
    {
        /// <summary>
        /// Function Creates a new Student
        /// </summary>
        /// <param name="student">An object of the new students details</param>
        /// <returns>The new students Id</returns>
        public static int CreateStudent(Student student, string password)
        {
            using (DataBaseContext databaseContext = new DataBaseContext())
            {
                var userId = CreateNewUserId();

                student.Id = userId;
                student.User_Id = userId;
                databaseContext.Student.Add(student);
                databaseContext.SaveChanges();

                databaseContext.User.Add(new User()
                {
                    Id = userId,
                    Password = password,
                    User_Id = userId,
                    Email = student.Email,
                    Count = 0,
                    LoginTimeStamp = DateTime.Now
                });
                databaseContext.SaveChanges();

                databaseContext.UserRole.Add(new UserRole()
                {
                    User_Id = userId,
                    IsActive = true,
                    Role = "student"
                });
                databaseContext.SaveChanges();

                return userId;
            }

        }

        private static int CreateNewUserId()
        {
            using (DataBaseContext databaseContext = new DataBaseContext())
            {
                int number;
                while (true)
                {
                    Random ran = new Random();
                    number = ran.Next(10000, 99999);
                    if (databaseContext.Student.Count(s => s.Id == number) == 0)
                        break;
                } 
                return number;
            }

        }
    }
}
