﻿using System;
using System.Collections.Generic;
using System.Linq;
using Models;

namespace DatabaseAccessLayer.LoginLogic
{
    /// <summary>
    /// This class is responsible for logging in
    /// </summary>
    public static class LoginManager
    {
        /// <summary>
        /// Function returns the users Id if they are a valid user
        /// </summary>
       /// <returns>The students Id</returns>
        public static string GetUserId(string email, string password)
        {
            using (DataBaseContext databaseContext = new DataBaseContext())
            {
                    IEnumerable<User> users = databaseContext.User;
                    var validUsers = users.Where(b => b.Email == email && b.Password == password);
                    if (validUsers.Count() != 1)
                        throw new Exception("bad log in request");
                    return validUsers.First().User_Id.ToString();
            }
        }
    }
}
