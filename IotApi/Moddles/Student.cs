﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Models
{
    public class Student
    {
        public string Email { get; set; }
        [Key]
        public int Id { get; set; }
        public int User_Id { get; set; }
        public string Name { get; set; }
        public long Contact { get; set; }
        public string Address { get; set; }
        public DateTime DOB { get; set; }
        public int Start_Date { get; set; }
        public int End_Date { get; set; }
        public int Course_Id { get; set; }
        public int Employer_Id { get; set; }
        public bool IsActive { get; set; }

    }
}
