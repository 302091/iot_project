﻿using System.ComponentModel.DataAnnotations;

namespace Models
{
    public class Course
    {
        [Key]
        public int Id { get; set; }
        public string Code { get; set; }
        public string Desc { get; set; }
        public string Type { get; set; }
        public string length { get; set; }
        public int Partner_Id { get; set; }
        public int Cost { get; set; }
        public bool IsActive { get; set; }
    }
}
