﻿using System.ComponentModel.DataAnnotations;

namespace Models
{
    public class UserRole
    {
        [Key]
        public int User_Id { get; set; }
        public string Role { get; set; }
        public bool IsActive { get; set; }

    }
}
