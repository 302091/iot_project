﻿using System.ComponentModel.DataAnnotations;

namespace Models
{
    public class Admin
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public long Contact { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public int Course_Id { get; set; }
        public int User_Id { get; set; }
        public bool IsActive { get; set; }
    }
}
