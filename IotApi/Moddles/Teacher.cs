﻿using System;
using System.Collections.Generic;

namespace Models
{
    public class Teacher
    {
        public int User_Id { get; set; }
        public string Email { get; set; }
        public string Id { get; set; }
        public string Name { get; set; }
        public long Contact { get; set; }
        public string Address { get; set; }
        public DateTime BirthDay { get; set; }
        public int Course_Id { get; set; }
        public bool IsActive { get; set; }
    }
}
