﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Models
{
    public class User 
    {
        [Key]
        public int Id { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public int Count { get; set; }
        public int User_Id { get; set; }

        public DateTime LoginTimeStamp { get; set; }
    }
}
